PDFLATEX     := pdflatex -shell-escape -interaction=nonstopmode -file-line-error
BIBTEX       := bibtex -min-crossrefs=9000
PS2PDF       := ps2pdf -dEPSCrop
RM           := rm -f

sources.tex  := $(wildcard *.tex)
styles.sty   := $(wildcard styles/*.sty)
biblio.bib   := $(wildcard biblio/*.bib)
images.dot   := $(wildcard images/*.dot)
images.pdf   := $(images.dot:%.dot=%.pdf)
main.pdf     := syspro-hw1.pdf
targets      := $(main.pdf)

dependencies := $(sources.tex) $(images.pdf)
dependencies += $(styles.sty) $(biblio.bib)

#--------------------------
# Default target
#--------------------------

.PHONY: all
all: $(targets)

$(targets): %.pdf: %.tex
	$(PDFLATEX) $<
	# $(BIBTEX) $*.aux
	$(PDFLATEX) $<
	$(PDFLATEX) $<

$(images.pdf): %.pdf: %.dot
	dot $< -Tpdf -o $@ 

#--------------------------
# Cleanup target
#--------------------------

.PHONY: clean
clean:
	$(RM) $(subst .pdf,.log,$(targets))
	$(RM) $(subst .pdf,.aux,$(targets))
	$(RM) $(subst .pdf,.bbl,$(targets))
	$(RM) $(subst .pdf,.blg,$(targets))
	$(RM) $(targets)


#--------------------------
# Additional dependencies
#--------------------------

$(main.pdf): $(dependencies)
